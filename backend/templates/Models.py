from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from flask_cors import CORS
from config import Config
from flask import Flask
import os

# initializing Flask app 
app = Flask(__name__)
app.app_context().push()
CORS(app)
app.config.from_object(Config)
db = SQLAlchemy(app)

class Book(db.Model):
    __tablename__ = 'book'

    id: int
    title: str
    publish_date: str
    page_count: int
    publisher_name: str
    author: str
    imageLink: str
    description: str

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False)
    publish_date = db.Column(db.String(200), nullable=False)
    page_count = db.Column(db.Integer)
    publisher_name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.String(2000), nullable=False)
    author = db.Column(db.String(200), nullable=False)
    imageLink = db.Column(db.String(500), nullable=False)

    def to_dict(self):
        return {key: value for key, value in self.__dict__.items() if not key.startswith('_')}

class Publisher(db.Model):
    __tablename__ = 'publisher'

    id: int
    name: str
    totalItems: int
    allBooksLink: str
    buyLink: str
    averageRating: float
    imageLink: str
    country: str

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500), nullable=False)
    totalItems = db.Column(db.Integer)
    allBooksLink = db.Column(db.String(500), nullable=False)
    buyLink = db.Column(db.String(500), nullable=False)
    averageRating = db.Column(db.Float)
    imageLink = db.Column(db.String(500), nullable=False)
    country = db.Column(db.String(500), nullable=False)


    def to_dict(self):
        return {key: value for key, value in self.__dict__.items() if not key.startswith('_')}

class Author(db.Model):
    __tablename__ = 'author'

    id: int
    name: str
    birth_date: str
    death_date: str
    work_count: int
    bio: str
    imageLink: str
    nationality: str

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    birth_date = db.Column(db.String(200), nullable=False)
    death_date = db.Column(db.String(200), nullable=True)
    work_count = db.Column(db.Integer)
    bio = db.Column(db.String(4000), nullable=False)
    imageLink = db.Column(db.String(500), nullable=False)
    nationality = db.Column(db.String(500), nullable=False)

    def to_dict(self):
        return {key: value for key, value in self.__dict__.items() if not key.startswith('_')}

db.create_all()