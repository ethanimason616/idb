import React from 'react';
import Dropdown from './Dropdown';

const SearchBar = ({ pageType, search, setSearch, sortBy, setSortBy, sortOrder, setSortOrder, filterBy, setFilterBy }) => {
    let sortOptions = [];
    let filterOptions = [];

    // Define options based on page type
    switch (pageType) {
        case 'books':
            sortOptions = [
                { value: "", label: "Sort By" },
                { value: "title", label: "Title" },
                { value: "page_count", label: "Page Count" }
            ];
            filterOptions = [
                { value: "", label: "Filter By" },
                { value: "1-200", label: "Page Count 1 to 200" },
                { value: "201-500", label: "Page Count 201 to 500" },
                { value: "501-1000", label: "Page Count 501 to 1000" },
                { value: ">1000", label: "Page Count More Than 1000" },
            ];
            break;
        case 'authors':
            sortOptions = [
                { value: "", label: "Sort By" },
                { value: "name", label: "Name" },
                { value: "work_count", label: "Work Count"}
            ];
            filterOptions = [
                { value: "", label: "Filter By" },
                { value: "living", label: "Currently Living" },
                { value: "not_living", label: "Not Currently Living" },
                { value: "american", label: "American Author"},
                { value: "british", label: "British Author"},
                { value: "other", label: "Other Nationalities"}
            ];
            break;
        case 'publishers':
            sortOptions = [
                { value: "", label: "Sort By" },
                { value: "name", label: "Name" },
                { value: "totalItems", label: "Number of Works" },
                { value: "averageRating", label: "Average Rating"}
            ];
            filterOptions = [
                { value: "", label: "Filter By" },
                { value: "works_gt_50", label: "Works > 50" },
                { value: "works_gt_100", label: "Works > 100" },
                { value: "works_gt_200", label: "Works > 200" },
                { value: "USA", label: "USA Publisher"},
                { value: "UK", label: "UK Publisher"},
                { value: "other", label: "Other Countries"}
            ];
            break;
        default:
            break;
    }

    return (
        <div className="search-bar-container">
            {/* Search input field */}
            <input 
                className="search-input"
                type="text" 
                placeholder="Search..." 
                value={search}
                onChange={(e) => setSearch(e.target.value)}
            />
            
            {/* Dropdown for sorting criteria */}
            <Dropdown 
                className="sort-dropdown"
                value={sortBy} 
                onChange={setSortBy} 
                options={sortOptions}
            />
            
            {/* Dropdown for sorting order */}
            <Dropdown 
                className="sort-dropdown"
                value={sortOrder} 
                onChange={setSortOrder} 
                options={[
                    { value: "asc", label: "Ascending" },
                    { value: "desc", label: "Descending" }
                ]}
            />

            {/* Dropdown for filtering criteria */}
            <Dropdown 
                className="filter-dropdown"
                value={filterBy} 
                onChange={setFilterBy} 
                options={filterOptions}
            />
        </div>
    );
};

export default SearchBar;
 
