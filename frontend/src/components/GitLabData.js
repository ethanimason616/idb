import axios from 'axios';

const gitlabToken = 'glpat-QL52wo5HyVZLhUGByspL';
const gitlabProjectId = '59251009';

const api = axios.create({
  baseURL: 'https://gitlab.com/api/v4',
  headers: {
    'Private-Token': gitlabToken,
  },
});

export const fetchClosedIssues = async (username) => {
  try {
    const response = await api.get(`/projects/${gitlabProjectId}/issues`, {
      params: {
        state: 'closed',
        per_page: 100, // increase if necessary
      },
    });

    const issues = response.data.filter((issue) => issue.closed_by && issue.closed_by.username === username);
    return issues.length;
  } catch (error) {
    console.error('Error fetching closed issues:', error);
    return 0;
  }
};

export const fetchTotalClosedIssues = async () => {
  try {
    const response = await api.get(`/projects/${gitlabProjectId}/issues`, {
      params: {
        state: 'closed',
        per_page: 100, // increase if necessary
      },
    });
    return response.data.length;
  } catch (error) {
    console.error('Error fetching closed issues:', error);
    return 0;
  }
};

export const fetchCommits = async (name) => {
  try {
    const response = await api.get(`/projects/${gitlabProjectId}/repository/commits`, {
      params: {
        state: 'closed',
        per_page: 100, // increase if necessary
      },
    });
    // console.log('Full response data:', response.data);
    const commits = response.data.filter((commit) => name.includes(commit.author_name));
    return commits.length;
  } catch (error) {
    console.error('Error fetching commits:', error);
    return 0;
  }
};

export const fetchTotalCommits = async () => {
  try {
    const response = await api.get(`/projects/${gitlabProjectId}/repository/commits`, {
      params: {
        state: 'closed',
        per_page: 100, // increase if necessary
      },
    });
    return response.data.length;
  } catch (error) {
    console.error('Error fetching commits:', error);
    return 0;
  }
};