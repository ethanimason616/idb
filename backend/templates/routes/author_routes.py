from flask import request, jsonify, Blueprint
from .. import db
from ..Models import Book, Author, Publisher
from sqlalchemy import and_, or_

author_bp = Blueprint("author_blueprint", __name__)

# Fetch all authors from the database with optional search and sort
@author_bp.route('/all', methods=['GET'])
def get_authors():
    search = request.args.get('search')
    sort_by = request.args.get('sort_by')
    sort_order = request.args.get('sort_order', 'asc')
    filter_by = request.args.get('filter_by')

    query = Author.query

    if search:
        query = query.filter(Author.name.ilike(f"%{search}%"))

    if sort_by:
        if sort_order == 'desc':
            query = query.order_by(db.desc(getattr(Author, sort_by)))
        else:
            query = query.order_by(getattr(Author, sort_by))
    
    if filter_by:
        if filter_by == 'living':
            query = query.filter(Author.death_date == 'N/A')
        elif filter_by == 'not_living':
            query = query.filter(Author.death_date != 'N/A')
        elif filter_by == 'american':
            query = query.filter(Author.nationality == 'American')
        elif filter_by == 'british':
            query = query.filter(Author.nationality == "British")
        elif filter_by == 'other':
            query = query.filter(and_(Author.nationality != 'American', Author.nationality != "British"))

    authors = query.all()
    return jsonify([author.to_dict() for author in authors])

# Fetch one author from the database
@author_bp.route('/<int:id>', methods=['GET'])
def get_author(id):
    author = Author.query.get_or_404(id)
    return jsonify(author.to_dict())

# Fetch one book by the author
@author_bp.route('/<int:id>/bookid', methods=['GET'])
def get_author_book(id):
    author = Author.query.get_or_404(id)
    author_name = author.name
    book = Book.query.filter(Book.author == author_name).first()
    if book is None:
        return jsonify({'id': 0})
    return jsonify({'id': book.id})

# Fetch one publisher by the author
@author_bp.route('/<int:id>/publisherid', methods=['GET'])
def get_author_publisher(id):
    author = Author.query.get_or_404(id)
    author_name = author.name
    book = Book.query.filter(Book.author == author_name).first()
    publisher_name = book.publisher_name
    publisher = Publisher.query.filter(Publisher.name == publisher_name).first()
    if publisher is None:
        return jsonify({'id':0})
    return jsonify({'id':publisher.id})