import json
import re

def clean_json(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as f:
        data = f.read()
    
    # Remove invalid control characters
    cleaned_data = re.sub(r'[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F]', '', data)
    
    # Try to load JSON data
    try:
        json_data = json.loads(cleaned_data)
    except json.JSONDecodeError as e:
        print(f"JSONDecodeError: {e}")
        with open("debug_output.json", "w", encoding="utf-8") as debug_file:
            debug_file.write(cleaned_data)
        raise
    
    # Iterate through each item and clean the 'bio' field
    for item in json_data:
        if 'bio' in item:
            item['bio'] = item['bio'].replace('\r', '').replace('\n', '')

    with open(output_file, 'w', encoding='utf-8') as f:
        json.dump(json_data, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    # input_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\authors_cleaned.json'
    # output_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\authors_cleaned.json'
    # clean_json(input_file_path, output_file_path)
    input_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\books_cleaned.json'
    output_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\books_cleaned.json'
    clean_json(input_file_path, output_file_path)
    # input_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\publishers.json'
    # output_file_path = 'C:\\Users\\ethan\\OneDrive\\Desktop\\School\\Summer24\\idb\\backend\\templates\\scraping\\publishers_cleaned.json'
    # clean_json(input_file_path, output_file_path)
