import Highlight from 'react-highlight-words';

function Highlighter({ search = '', text = '' }) {
    // Convert search to an array if it's not already
    const wordsToHighlight = Array.isArray(search) ? search : [search];

    return (
        <Highlight
            searchWords={wordsToHighlight}
            textToHighlight={text}
            highlightClassName="highlight"
        />
    );
}

export default Highlighter;
