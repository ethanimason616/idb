import React, { useState, useEffect } from 'react';
import TeamMember from '../components/TeamMember';
import Tools from '../components/Tools.js'
import '../styles/About.css';
import '../styles/Tools.css';
import { fetchClosedIssues, fetchCommits,  fetchTotalClosedIssues, fetchTotalCommits} from '../components/GitLabData.js';
import ethanHeadshot from '../images/ethan-headshot.jpg';
import nerwenHeadshot from '../images/nerwen-headshot.jpg'; 
import hannahHeadshot from '../images/hannah-headshot.jpg';
import kathyHeadshot from '../images/kathy.png';
import sohilHeadshot from '../images/sohil-headshot.jpg';
import gitLabLogo from '../images/gitlab-logo.jpg';
import bootstrapLogo from '../images/Bootstrap_logo.png';
import postmanLogo from '../images/postman-logo.jpg';
import reactLogo from '../images/React-icon.png';
import VSCodeLogo from '../images/vscode-logo.png';
import discordLogo from '../images/discord_logo.jpg';
import gcpLogo from '../images/gcp_logo.jpg';
import postgresqlLogo from '../images/postgresql_logo.jpg';
import pythonLogo from '../images/python_logo.jpg';


const teamData = [
  {
    name: 'Ethan Mason',
    commit_author_name: ['Ethan Mason'],
    role: 'Back-end',
    description: 'I\'m a junior CS major at UT Austin, minoring in French. My hobbies include Muay Thai, video games, and reading.',
    image: ethanHeadshot,
    gitLabID: 'ethanimason616',
    unitTests: 3

  },
  {
    name: 'Nerwen Cao',
    commit_author_name: ['Nerwen Cao', 'NerwenC'],
    role: 'Full-stack',
    description: 'I\'m a junior CS major at UT Austin. In my free time, I enjoy curating Spotify playlists and watching movies.',
    image: nerwenHeadshot,
    gitLabID: 'nerwen11',
    issuesClosed: 0,
    unitTests: 2
  },
  {
    name: 'Hannah Hughes',
    commit_author_name: ['hannahmhughes', 'Hannah Hughes'],
    role: 'Back-end/DevOps',
    description: 'I\'m a senior CS major at UT Austin with a minor in History. I enjoy hiking, kayaking, and reading.',
    image: hannahHeadshot,
    gitLabID: 'hannahmhughes',
    unitTests: 2
  },
  {
    name: 'Katherine Liang',
    commit_author_name: ['Katherine Liang'],
    role: 'Front-end',
    description: 'I\'m a senior CS, Math, and Japanese student who also works as a TA. I love playing with my cat, playing videos games, and working out. (◕‿◕)',
    image: kathyHeadshot,
    gitLabID: 'katherineliang100',
    unitTests: 1
  },
  {
    name: 'Sohil Patel',
    commit_author_name: ['Sohil Patel'],
    role: 'Full-stack',
    description: 'I\'m a junior CS major at UT Austin. Some of my hobbies are golf, football, and cooking.',
    image: sohilHeadshot,
    gitLabID: 'spatel0319',
    unitTests: 1
  },
];

const logos = [
  {
    image: gitLabLogo,
    name: 'GitLab'
  },
  {
    image: reactLogo,
    name: 'React'
  }, 
  {
    image: bootstrapLogo,
    name: 'Bootstrap'
  }, 
  {
    image: postmanLogo,
    name: 'Postman'
  },
  {
    image: pythonLogo,
    name: 'Python'
  },
  {
    image: postgresqlLogo,
    name: 'PostgreSQL'
  },
  {
    image: gcpLogo,
    name: 'GCP'
  },
  {
    image: discordLogo,
    name: 'Discord'
  },
  {
    image: VSCodeLogo,
    name: 'VSCode'
  }];

const About = () => {
  const [team, setTeam] = useState(teamData);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const updatedTeam = await Promise.all(
        team.map(async (member) => {
          if (!member.gitLabID) return member;
          const issuesClosed = await fetchClosedIssues(member.gitLabID);
          const commits = await fetchCommits(member.commit_author_name);
          // const unitTests = await fetchUnitTestResults(member.gitLabID);
          return {
            ...member,
            issuesClosed,
            commits,
            //unitTests,
          };
        })
      );
      setTeam(updatedTeam);
      const totalCommits = await fetchTotalCommits();
      const totalIssues = await fetchTotalClosedIssues();
      setTotalCommits(totalCommits);
      setTotalIssues(totalIssues);
    };

    fetchData();
  }, []);

  return (
    <div className='page-container'>
      <h1>Our Team</h1>
      <div className='team-card-container'>
        {team.map((member) => (
          <TeamMember
            name={member.name}
            role={member.role}
            description={member.description}
            image={member.image}
            gitLabID={member.gitLabID}
            issuesClosed={member.issuesClosed}
            commits={member.commits}
            unitTests={member.unitTests}
          />
        ))}
        <br></br>
        <br></br>
      </div>
      <h1>Stats</h1>
      <div>
        <p>Total No. of Commits: {totalCommits}</p>
        <p>Total No. of Issues: {totalIssues}</p>
        <p>Total No. of Unit Tests: 9</p>
        <p>API: <a href='https://database.booklore.software/api'>https://database.booklore.software/api</a></p>
        <p>GitLab Repo: <a href='https://gitlab.com/ethanimason616/idb'>https://gitlab.com/ethanimason616/idb</a></p>
        <p>GitLab Wiki: <a href='https://gitlab.com/ethanimason616/idb/-/wikis/BookLore-IDB'>https://gitlab.com/ethanimason616/idb/-/wikis/BookLore-IDB</a></p>
        <p>GitLab Issue Tracker: <a href='https://gitlab.com/ethanimason616/idb/-/issues'>https://gitlab.com/ethanimason616/idb/-/issues</a></p>
        <p>Speaker Deck: <a href='https://speakerdeck.com/spatel03/booklore'>https://speakerdeck.com/spatel03/booklore</a></p>
      </div>
      <br></br>
      <br></br>
      <h1>Data</h1>
      <div>
        <p>Postman API Documentation: <a href='https://documenter.getpostman.com/view/36672960/2sA3dvjsWw'>https://documenter.getpostman.com/view/36672960/2sA3dvjsWw</a></p>
        <p>Data Sources:</p>
        <p><a href='https://www.googleapis.com/books'>https://www.googleapis.com/books</a></p>
        <p><a href='https://openlibrary.org/authors'>https://openlibrary.org/authors</a></p>
        <p><a href='https://www.bing.com/images/'>https://www.bing.com/images/</a></p>
        <p>Description of how each was scraped: </p>
        <p>Used GET methods to retrieve information from the Open Library API and the Google Books API. Used BeautifulSoup to scrape images.</p>
      </div>
      <br></br>
      <br></br>
      <h1>Tools</h1>
      <div className='tools-container'>
        {logos.map((logo, index) => (
          <Tools
            key={index}
            image={logo.image}
            name={logo.name}
          />
        ))}
      </div>
    </div>
  );
};

export default About;
