import React from "react";
import { NavLink } from "react-router-dom";
import '../styles/Navbar.css';

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark ">
      <div className="container-fluid">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink className="nav-link" to="/">Welcome</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/about">About</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/books">Books</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/authors">Authors</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/publishers">Publishers</NavLink>
          </li>
        </ul>
      </div>
    </nav>
    
  );
};

export default Navbar;
