import React from 'react';
import '../styles/Tools.css'

const Resource = ({ name, image }) => {
  return (
    <div className="card-tools">
      <img className="card-img-top tools-logo" src={image} alt="card image"/>
      <div className="card-body-tools"><h4 className="card-text">{name}</h4></div>
    </div>
  );
};

export default Resource;