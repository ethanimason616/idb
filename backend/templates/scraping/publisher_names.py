import json

# Load the JSON data from file
with open('books.json', 'r', encoding='utf-8') as f:
    data = json.load(f)

# Initialize as set to prevent duplicates
publishers = set()

# Iterate over each book entry in the JSON data and extract the publisher
for book in data:
    publishers.add(book['publisher_name'])

publishers = list(publishers)

with open('publisher_names.json', 'w') as file:
    json.dump(publishers, file, indent=4)

# Now 'publishers' list contains all the publishers from the JSON data