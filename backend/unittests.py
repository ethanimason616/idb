from main import create_app
from unittest import main, TestCase
import requests
app = create_app()


class ModelTests(TestCase):
    def __init__(self):
        self.URL = 'https://database.booklore.software/api'                         
    
    ### ------------ ###
    ### Test our API ###
    ### ------------ ###


    def test_one_book(self):
        response = requests.get(self.BASE_URL + "/books/1")
        data = response.json()
        expected_keys = {
            "id",
            "title",
            "publish_date",
            "page_count",
            "publisher_name",
            "description",
            "author",
            "imageLink"
        }
        self.assertSetEqual(set(data.keys()), expected_keys)




   
    def test_one_author(self):
        response = requests.get(self.BASE_URL + "/authors/1")
        data = response.json()
        expected_keys = {
            "id",
            "name",
            "birth_date",
            "death_date",
            "work_count",
            "bio",
            "imageLink"
        }
        self.assertSetEqual(set(data.keys()), expected_keys)


    def test_one_publisher(self):
        response = requests.get(self.BASE_URL + "/publishers/1")
        data = response.json()
        expected_keys = {
            "id",
            "name",
            "totalItems",
            "allBooksLink",
            "buyLink",
            "averageRating",
            "imageLink"
        }
        self.assertSetEqual(set(data.keys()), expected_keys)




    def test_all_books(self):
        response = requests.get(self.BASE_URL + "/books/all")
        self.assertEqual(response.status_code, 200)
   
    def test_all_authors(self):
        response = requests.get(self.BASE_URL + "/authors/all")
        self.assertEqual(response.status_code, 200)


    def test_all_publishers(self):
        response = requests.get(self.BASE_URL + "/publishers/all")
        self.assertEqual(response.status_code, 200)




if __name__ == '__main__':
    main()