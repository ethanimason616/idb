import React, { useState, useEffect } from 'react';
import '../styles/ModelPage.css';
import BookCard from '../components/BookCard';
import Pagination from '../components/Pagination';
import urls from '../url.json';
import SearchBar from '../components/SearchBar';

function Books() {
    const [data, setData] = useState([]);
    const [fetchSuccess, setFetchSuccess] = useState(false);
    const [currentPage, setCurrentPage] = useState(0);
    const itemsPerPage = 9;
    const [search, setSearch] = useState('');
    const [sortBy, setSortBy] = useState('');
    const [sortOrder, setSortOrder] = useState('asc');
    const [filterBy, setFilterBy] = useState('');

    const fetchData = async (search = '', sortBy = '', sortOrder = 'asc', filterBy = '') => {
        try {
            const response = await fetch(`${urls.backendURL}api/books/all?search=${search}&sort_by=${sortBy}&sort_order=${sortOrder}&filter_by=${filterBy}`);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonData = await response.json();
            setData(jsonData);
            setFetchSuccess(true);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData(search, sortBy, sortOrder, filterBy);
    }, [search, sortBy, sortOrder, filterBy]);

    const totalPages = Math.ceil(data.length / itemsPerPage);
    const currentPageData = data.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);

    const updatePage = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    return (
        <div className='page-container'>
            <h1>Books</h1>
            <SearchBar 
                pageType="books"
                search={search} 
                setSearch={setSearch} 
                sortBy={sortBy} 
                setSortBy={setSortBy} 
                sortOrder={sortOrder} 
                setSortOrder={setSortOrder}
                filterBy={filterBy}
                setFilterBy={setFilterBy}
            />
            <p>Found {data.length} books</p>
            <div className='card-container'>
                {currentPageData.map((book) => (
                    <BookCard key={book.id} data={book} search={search} />
                ))}
            </div>
            <Pagination
                currentPage={currentPage}
                updatePage={updatePage}
                totalPages={totalPages}
            />
        </div>
    );
}

export default Books;
