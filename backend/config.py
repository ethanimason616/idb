import os

googleApiKey = 'AIzaSyCmvxWGLO801yhTy0O5T-CjQpLo2s813BI'
cx = '305ebc0d18267427c'

# Ethan's local db
# class Config:
    # USER = 'postgres'
    # PASSWORD = '9OYNW3i6u320'
    # SERVER = 'localhost'
    # PORT = '5432'
    # DB_NAME = 'bookdb'
    # SQLALCHEMY_DATABASE_URI = f'postgresql://{USER}:{PASSWORD}@{SERVER}:{PORT}/{DB_NAME}'
    # SQLALCHEMY_TRACK_MODIFICATIONS = False

# Nerwen's local db
class Config:
    USER = 'postgres'
    PASSWORD = 'darkmodenobugs'
    SERVER = 'localhost'
    PORT = '5432'
    DB_NAME = 'bookdb8'
    SQLALCHEMY_DATABASE_URI = f'postgresql://{USER}:{PASSWORD}@{SERVER}:{PORT}/{DB_NAME}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
# Kathy's local db
# class Config:
#     USER = 'postgres'
#     PASSWORD = 'mynewlaptop'
#     SERVER = 'localhost'
#     PORT = '5432'
#     DB_NAME = 'kawaiidb'
#     SQLALCHEMY_DATABASE_URI = f'postgresql://{USER}:{PASSWORD}@{SERVER}:{PORT}/{DB_NAME}'
#     SQLALCHEMY_TRACK_MODIFICATIONS = False

#Hannah's Local DB
# class Config:
#     USER = 'postgres'
#     PASSWORD = 'pass'
#     SERVER = 'localhost'
#     PORT = '5432'
#     DB_NAME = 'db'
#     SQLALCHEMY_DATABASE_URI = f'postgresql://{USER}:{PASSWORD}@{SERVER}:{PORT}/{DB_NAME}'
#     SQLALCHEMY_TRACK_MODIFICATIONS = False

# Cloud config
# class Config:
#     USER = 'postgres'
#     PASSWORD = 'password123'
#     SERVER = '34.46.56.218'
#     PORT = '5432'
#     DB_NAME = 'db'
#     SQLALCHEMY_DATABASE_URI = f'postgresql://{USER}:{PASSWORD}@{SERVER}:{PORT}/{DB_NAME}'
#     SQLALCHEMY_TRACK_MODIFICATIONS = False