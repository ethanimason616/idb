import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import urls from '../url.json';

function OneAuthor() {
    const { authorId } = useParams();
    const [author, setAuthor] = useState(null);
    const [fetchSuccess, setFetchSuccess] = useState(false);
    const [bookLink, setBookLink] = useState(null);
    const [publisherLink, setPublisherLink] = useState(null);

    useEffect(() => {
        const fetchAuthor = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/authors/${authorId}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch author data');
                }
                const authorData = await response.json();
                setAuthor(authorData);
                setFetchSuccess(true);
            } catch (error) {
                console.error('Error fetching author data:', error);
            }
        };

        const fetchBookLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/authors/${authorId}/bookid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const bookID  = jsonData.id;
                setBookLink(`${urls.frontendURL}books/${bookID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        const fetchPublisherLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/authors/${authorId}/publisherid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const publisherID  = jsonData.id;
                setPublisherLink(`${urls.frontendURL}publishers/${publisherID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchAuthor();
        fetchBookLink();
        fetchPublisherLink();
    }, [authorId]);

    return (
        <div className='page-container'>
            {fetchSuccess && author ? (
                <>
                    <h1>{author.name}</h1>
                    <br></br>
                    <img src={author.imageLink} alt="Author Image" />
                    <br></br>
                    <br></br>
                    <p>Nationality: {author.nationality}</p>
                    <p>Birth Date: {author.birth_date}</p>
                    <p>Death Date: {author.death_date}</p>
                    <p>Work Count: {author.work_count}</p>
                    <p>Bio: {author.bio}</p>
                    <p>One Book by This Author: <a href={bookLink}>{bookLink}</a></p>
                    <p>One Publisher This Author Used: <a href={publisherLink}>{publisherLink}</a></p>
                </>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );
}

export default OneAuthor;
