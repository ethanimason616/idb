import requests
import json
from open_library_identifiers import author_identifiers
from bs4 import BeautifulSoup
from urllib.parse import quote

def fetch_image_url(query, num=1):
    query = quote(query)
    search_url = f"https://www.bing.com/images/search?q={query}&first={num}"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }
    
    response = requests.get(search_url, headers=headers)
    soup = BeautifulSoup(response.content, 'html.parser')
    
    img_tags = soup.findAll('img', class_='mimg')
    if img_tags:
        return img_tags[0]['src']
    
    return ''

def fetch_authors():
    all_data = []
    for author_id in author_identifiers.values():
        author_data = {}
        url = f'https://openlibrary.org/authors/{author_id}.json'
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            author_data['birth_date'] = data.get('birth_date')
            author_data['death_date'] = data.get('death_date')
            author_data['name'] = data.get('name')
            author_data['work_count'] = fetch_work_count(author_id)
            author_data['imageLink'] = fetch_image_url(data.get('name'))
            if 'bio' in data:
                bio = data.get('bio')
                if 'value' in bio:
                    author_data['bio'] = bio.get('value')
                else:
                    author_data['bio'] = bio
            else:
                author_data['bio'] = ""
            all_data.append(author_data)
        else:
            print(f"Failed to get data for ID {author_id}. Status code {response.status_code}")
    return all_data


def fetch_work_count(author_id):
        url = f'https://openlibrary.org/authors/{author_id}/works.json'
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()  # assuming data is a dictionary with a 'size' field
            size = data.get('size')
            if size is not None:
                return size
        else:
            print(f"Failed to get data for ID {author_id}. Status code {response.status_code}")




def save_to_json(data, filename):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=4)

        
authors_data = fetch_authors()
save_to_json(authors_data, 'authors.json')

# work_counts = fetch_work_count()
# save_to_json(work_counts, 'work_count.json')
