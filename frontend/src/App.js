import React from 'react';
import Banner from './components/Banner';
import Navbar from './components/Navbar';
import Splash from './pages/Splash';
import About from './pages/About';
import Books from './pages/Books';
import Authors from './pages/Authors';
import Publishers from './pages/Publishers';
import OneBook from './pages/oneBook'; 
import OneAuthor from './pages/oneAuthor'; 
import OnePublisher from './pages/onePublisher'; 
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


const App = () => {
  return (
    <Router>
      <div>
        <Banner />
        <Navbar />
        <div className="content">
          <Routes>
            <Route path="/" element={<Splash />} />
            <Route path="/about" element={<About />} />
            <Route path="/books" element={<Books />} />
            <Route path="/authors" element={<Authors />} />
            <Route path="/publishers" element={<Publishers />} />
            <Route path="/books/:bookId" element={<OneBook />} />
            <Route path="/authors/:authorId" element={<OneAuthor />} />
            <Route path="/publishers/:publisherId" element={<OnePublisher />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;
