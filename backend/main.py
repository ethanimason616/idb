from flask import Flask
from flask_cors import CORS
from config import Config
from create_db import app, db, Book, Author, Publisher, populate_authors, populate_books, populate_publishers
from templates.routes.book_routes import book_bp
from templates.routes.author_routes import author_bp
from templates.routes.publisher_routes import publisher_bp


app.register_blueprint(book_bp, url_prefix='/api/books') 
app.register_blueprint(author_bp, url_prefix='/api/authors')
app.register_blueprint(publisher_bp, url_prefix='/api/publishers')

@app.route('/')
def index():
    return "Hello World"

@app.route("/api", methods=["GET"])
def home():
    return "Welcome to BookLore API"

#config_app()
#app.run(debug=True)

if __name__ == "__main__":
	app.run(host='0.0.0.0')
