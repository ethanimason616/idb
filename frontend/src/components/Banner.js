import React from 'react';
import { NavLink } from "react-router-dom";
import '../styles/Banner.css';
import bannerImage from '../images/banner.jpg'

function Banner() {
  return (
    <div className="banner">
      <img src={bannerImage} alt="Banner" className="banner-image" />
      <div className="banner-content">
        <NavLink className="nav-link" to ="/">BookLore</NavLink>
      </div>
    </div>
  );
}

export default Banner;
