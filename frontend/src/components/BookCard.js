import React from 'react';
import '../styles/Card.css';
import { Link } from 'react-router-dom';
import Highlighter from './Highlighter';

const BookCard = ({ data, search }) => {

    // Function to extract the first two lines from the text
    const getFirstSentence = (text) => {
        if (text) {
            return text.substring(0, 140);
        }
        return ''; // Return empty string if no text provided
    };

    return (
        <div className="card">
            <img className="card-img-top image" src={data.imageLink} alt="Card image"/>
            <div className="card-body">
                <h4 className='card-title'><Highlighter search={search} text={data.title}/></h4>
                <p className="card-text"><Highlighter search={search} text={`Author: ${data.author}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Publisher: ${data.publisher_name}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Publish Date: ${data.publish_date}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Page Count: ${data.page_count}`}/></p>
                {/* Display only the first sentence of the bio */}
                <p className="card-text"><Highlighter search={search} text={`Description:: ${getFirstSentence(data.description)}`}/></p>
            </div>
            <div className='card-footer'>
            <Link to={`/books/${data.id}`} className="btn btn-custom">More Info</Link>  
            </div>
        </div>
    );
};

export default BookCard;
