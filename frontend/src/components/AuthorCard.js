import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/Card.css';
import Highlighter from './Highlighter';

const AuthorCard = ({ data, search  }) => {
    // Function to extract the first sentence from the bio
    const getFirstSentence = (text) => {
        if (text) {
            // Split the text into sentences based on common ending punctuation
            const sentences = text.split(/[.!?]/);
            // Return the first sentence
            return sentences[0];
        }
        return ''; // Return empty string if no text provided
    };

    return (
        <div className="card">
            <img className="card-img-top image" src={data.imageLink} alt="Card image"/>
            <div className="card-body">
                <h4 className='card-title'><Highlighter search={search} text={data.name}/></h4>
                <p className="card-text"><Highlighter search={search} text={`Nationality: ${data.nationality}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Birth Date: ${data.birth_date}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Death Date: ${data.death_date}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Work Count: ${data.work_count}`}/></p>
                {/* Display only the first sentence of the bio */}
                <p className="card-text"><Highlighter search={search} text={`Bio: ${getFirstSentence(data.bio)}`}/></p>
            </div>
            <div className='card-footer'>
                <Link to={`/authors/${data.id}`} className="btn btn-custom">More Info</Link>
            </div>
        </div>
    );
};

export default AuthorCard;
