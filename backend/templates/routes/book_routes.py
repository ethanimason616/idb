from flask import request, jsonify, Blueprint
from .. import db
from ..Models import Book, Author, Publisher
from sqlalchemy import and_

book_bp = Blueprint("book_blueprint", __name__)

# Fetch all books from the database
@book_bp.route('/all', methods=['GET'])
def get_books():
    search = request.args.get('search')
    sort_by = request.args.get('sort_by')
    sort_order = request.args.get('sort_order', 'asc')
    filter_by = request.args.get('filter_by')

    query = Book.query

    if search:
        query = query.filter(Book.title.ilike(f"%{search}%"))
        
    if sort_by:
        if sort_order == 'desc':
            query = query.order_by(db.desc(getattr(Book, sort_by)))
        else:
            query = query.order_by(getattr(Book, sort_by))
    
    if filter_by:
        if filter_by == '1-200':
            query = query.filter(and_(Book.page_count >= 1, Book.page_count <= 200))
        elif filter_by == '201-500':
            query = query.filter(and_(Book.page_count >= 201, Book.page_count <= 500))
        elif filter_by == '501-1000':
            query = query.filter(and_(Book.page_count >= 501, Book.page_count <= 1000))
        elif filter_by == '>1000':
            query = query.filter(and_(Book.page_count >= 1000))

    books = query.all()
    return jsonify([book.to_dict() for book in books])

# Fetch one book from the database
@book_bp.route('/<int:id>', methods=['GET'])
def get_book(id):
    book = Book.query.get_or_404(id)
    return jsonify(book.to_dict())

# Fetch author ID for the book
@book_bp.route('/<int:id>/authorid', methods=['GET'])
def get_book_author(id):
    book = Book.query.get_or_404(id)
    author_name = book.author
    author = Author.query.filter(Author.name == author_name).first()
    if author is None:
        return jsonify({'id': 0})
    return jsonify({'id': author.id})

# Fetch publisher ID for the book
@book_bp.route('/<int:id>/publisherid', methods=['GET'])
def get_book_publisher(id):
    book = Book.query.get_or_404(id)
    publisher_name = book.publisher_name
    publisher = Publisher.query.filter(Publisher.name == publisher_name).first()
    if publisher is None:
        return jsonify({'id':0})
    return jsonify({'id':publisher.id})
