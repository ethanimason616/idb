import json
from templates.Models import app, db, Book, Author, Publisher

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def populate_books():
    books_data = load_json('./templates/scraping/books_cleaned.json')
    i = 1
    for book_info in books_data:
        imageLink = book_info["imageLink"]
        if imageLink == "No Link Available":
            continue
        title = book_info["title"]
        publish_date = book_info["publish_date"]
        page_count = book_info["page_count"]
        publisher_name = book_info["publisher_name"]
        description = book_info["description"]
        if len(description) > 1900:
            description = description[:1900]
            description += "..."
        author = book_info["author"]
        
        new_book = Book(
            id=i,
            title=title,
            publish_date=publish_date,
            page_count=page_count,
            publisher_name=publisher_name,
            description=description,
            author=author,
            imageLink=imageLink
        )

        db.session.add(new_book)
        i += 1
    db.session.commit()

def populate_authors():
    authors_data = load_json('./templates/scraping/authors_cleaned.json')  
    i = 1
    for j, oneAuthor in enumerate(authors_data):
        bio = oneAuthor['bio']
        name = oneAuthor['name']
        birth_date = oneAuthor['birth_date']
        death_date = oneAuthor['death_date']
        bio = oneAuthor['bio']
        if len(bio) > 1900:
            bio = bio[:1900]
            bio += "..."
        work_count = oneAuthor['work_count']
        imageLink = oneAuthor['imageLink']
        nationality = oneAuthor['nationality']
        id = i

        newAuthor = Author(name=name, birth_date=birth_date, death_date=death_date, work_count=work_count, 
                           bio=bio, imageLink=imageLink, nationality=nationality, id=id)
        db.session.add(newAuthor)
        i += 1
    db.session.commit()

def populate_publishers():
    publishers_data = load_json('./templates/scraping/publishers_cleaned.json')
    i = 1
    for onePublisher in publishers_data:
        name = onePublisher['name']
        totalItems = onePublisher['totalItems']
        allBooksLink = onePublisher['allBooksLink']
        buyLink = onePublisher['buyLink']
        averageRating = onePublisher['averageRating']
        imageLink = onePublisher['imageLink']
        country = onePublisher['country']
        id = i

        newPublisher = Publisher(name=name, totalItems=totalItems, allBooksLink=allBooksLink, buyLink=buyLink,
                                 averageRating=averageRating, imageLink=imageLink, country=country, id=id)

        db.session.add(newPublisher)
        i += 1
    db.session.commit()
        
db.drop_all()
db.create_all()  # Ensure all tables are created

populate_books()
populate_authors()
populate_publishers()