from flask import request, jsonify, Blueprint
from .. import db
from ..Models import Book, Author, Publisher
from sqlalchemy import and_

publisher_bp = Blueprint("publisher_blueprint", __name__)

# Fetch all publishers from the database
@publisher_bp.route('/all', methods=['GET'])
def get_publishers():
    search = request.args.get('search')
    sort_by = request.args.get('sort_by')
    sort_order = request.args.get('sort_order', 'asc')
    filter_by = request.args.get('filter_by')

    query = Publisher.query

    if search:
        query = query.filter(Publisher.name.ilike(f"%{search}%"))

    if sort_by:
        if sort_order == 'desc':
            query = query.order_by(db.desc(getattr(Publisher, sort_by)))
        else:
            query = query.order_by(getattr(Publisher, sort_by))

    if filter_by:
        if filter_by == 'works_gt_50':
            query = query.filter(Publisher.totalItems > 50)
        elif filter_by == 'works_gt_100':
            query = query.filter(Publisher.totalItems > 100)
        elif filter_by == 'works_gt_200':
            query = query.filter(Publisher.totalItems > 200)
        elif filter_by == 'USA':
            query = query.filter(Publisher.country == 'USA')
        elif filter_by == 'UK':
            query = query.filter(Publisher.country == "UK")
        elif filter_by == 'other':
            query = query.filter(and_(Publisher.country != 'USA', Publisher.country != "UK"))

    publishers = query.all()
    return jsonify([publisher.to_dict() for publisher in publishers])

# Fetch one publisher from the database
@publisher_bp.route('/<int:id>', methods=['GET'])
def get_publisher(id):
    publisher = Publisher.query.get_or_404(id)
    return jsonify(publisher.to_dict())

# Fetch one book by the publisher
@publisher_bp.route('/<int:id>/bookid', methods=['GET'])
def get_publisher_book(id):
    publisher = Publisher.query.get_or_404(id)
    publisher_name = publisher.name
    book = Book.query.filter(Book.publisher_name == publisher_name).first()
    if book is None:
        return jsonify({'id': 0})
    return jsonify({'id': book.id})

# Fetch one author by the publisher
@publisher_bp.route('/<int:id>/authorid', methods=['GET'])
def get_publisher_author(id):
    publisher = Publisher.query.get_or_404(id)
    publisher_name = publisher.name
    book = Book.query.filter(Book.publisher_name == publisher_name).first()
    author_name = book.author
    author = Author.query.filter(Author.name == author_name).first()
    if author is None:
        return jsonify({'id': 0})
    return jsonify({'id': author.id})