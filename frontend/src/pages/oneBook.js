import React, { useState, useEffect } from 'react';
// Import useParams hook to access route parameters
import { useParams } from 'react-router-dom'; 
import urls from '../url.json';

function OneBook() {
    const { bookId } = useParams(); // Destructure bookId from URL parameters using useParams
    const [book, setBook] = useState(null); // State to hold the book data
    const [fetchSuccess, setFetchSuccess] = useState(false); // State to track if data fetching was successful
    const [authorLink, setAuthorLink] = useState(null);
    const [publisherLink, setPublisherLink] = useState(null);

    useEffect(() => {
        // useEffect hook to fetch book data when component mounts or bookId changes
        const fetchBook = async () => {
            try {
                // Fetch book data from API using bookId parameter from URL
                const response = await fetch(`${urls.backendURL}api/books/${bookId}`);
                
                if (!response.ok) {
                    throw new Error('Failed to fetch data'); // Throw error if response is not ok
                }
                
                const bookData = await response.json(); // Parse JSON response
                setBook(bookData); // Set book data in state
                setFetchSuccess(true); // Set fetch success flag to true
            } catch (error) {
                console.error('Error fetching data:', error); // Log error if fetch fails
            }
        };

        const fetchAuthorLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/books/${bookId}/authorid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const authorID  = jsonData.id;
                setAuthorLink(`${urls.frontendURL}authors/${authorID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        const fetchPublisherLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/books/${bookId}/publisherid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const publisherID  = jsonData.id;
                setPublisherLink(`${urls.frontendURL}publishers/${publisherID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchBook(); // Call fetchBook function when component mounts or bookId changes
        fetchAuthorLink();
        fetchPublisherLink();
    }, [bookId]); // useEffect dependency array, re-runs effect when bookId changes


    return (
        <div className='page-container'>
            {fetchSuccess && book ? ( // Conditional rendering based on fetch success and book data availability
                <>
                    <h1>{book.title}</h1> {/* Display book title */}
                    <br></br>
                    <img src={book.imageLink} alt="Book Image" />
                    <br></br>
                    <br></br>
                    <p>Author: <a href={authorLink}>{book.author}</a></p> {/* Display book author */}
                    <p>Publisher: <a href={publisherLink}>{book.publisher_name}</a></p> {/* Display publisher name */}
                    <p>Publish Date: {book.publish_date}</p> {/* Display publish date */}
                    <p>Page Count: {book.page_count}</p> {/* Display page count */}
                    <p>Description: {book.description}</p> {/* Display description */}
                </>
            ) : (
                <p>Loading...</p> // Display loading message while data is being fetched
            )}
        </div>
    );
}

export default OneBook; 

