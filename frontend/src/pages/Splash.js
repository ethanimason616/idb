import React from 'react';
import '../styles/Splash.css';
import splashImage from '../images/splash.jpg'

const Splash = () => {
  return (
    <div className="page-container">
      <div className="row">
        <div className="col-md-6">
          <img src={splashImage} alt="splash" className="splash-image" />
        </div>
        <div className="col-md-6">
          <div className="text-container">
            <h1>Welcome to BookLore</h1>
            <br></br>
            <h4>— discover your next favorite book —</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Splash;
