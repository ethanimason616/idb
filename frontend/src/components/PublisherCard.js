import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/Card.css';
import Highlighter from './Highlighter';

const PublisherCard = ({ data, search  }) => {
    return (
        <div className="card">
            <img className="card-img-top image" src={data.imageLink} alt="Card image"/>
            <div className="card-body">
                <h4 className='card-title'><Highlighter search={search} text={data.name}/></h4>
                <p className="card-text"><Highlighter search={search} text={`Country: ${data.country}`}/></p>
                <p className="card-text"><Highlighter search={search} text={`Average Rating: ${data.averageRating}`}/></p>
                <p className="card-text">: {data.averageRating}</p>
                <p className="card-text"><Highlighter search={search} text={`Book Count: ${data.totalItems}`}/></p>
                <p className="card-text">Link to Books: <a href={data.allBooksLink}>{data.allBooksLink}</a></p>
                <p className="card-text">Link to Purchase: <a href={data.buyLink}>{data.buyLink}</a></p>

            </div>
            <div className='card-footer'>
                <Link to={`/publishers/${data.id}`} className="btn btn-custom">More Info</Link>
            </div>
        </div>
    );
};

export default PublisherCard;
