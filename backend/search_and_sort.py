import sys
sys.path.append('C:/Users/ethan/OneDrive/Desktop/School/Summer24/idb/backend')
import requests



BASE_URL = 'https://database.booklore.software/api'


def fetch_data(endpoint):
    response = requests.get(BASE_URL + endpoint)
    if response.status_code == 200:
        return response.json()  # Extract JSON data from the response
    else:
        response.raise_for_status()




# Fetch data from the database
authors = fetch_data('authors/all')
books = fetch_data('books/all')
publishers = fetch_data('publishers/all')


# Function to sort a list of dictionaries alphabetically by a specified key
def sort_alphabetically(data, key):
    return sorted(data, key=lambda x: x[key].lower())


# Function to sort a list of dictionaries numerically by a specified key
def sort_books_by_year(data, key):
    return sorted(data, key=lambda x: x[key][:4])


# Filters a list of dictionaries based on a specified key and value.
# Parameters:
# data: List of dictionaries to be filtered.
# key: The key in the dictionary to filter by.
# value: The value to filter by.
def filter_by_key_value(data, key, value):
    return [item for item in data if item.get(key) == value]




# Function to search for a specific item in a list of dictionaries by a specified key and value
def search_item(data, key, value):
    return next((item for item in data if item[key].lower() == value.lower()), None)



def filter_living_authors():
        print(filter_by_key_value(authors, 'death_date', 'N/A'))

