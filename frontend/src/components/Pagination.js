import { Link } from 'react-router-dom';
import { GrFormPrevious, GrFormNext } from 'react-icons/gr';

// We have 3 props: currentPage number, function to update page number,
// and the total number of pages
function Pagination({ currentPage, updatePage, totalPages }) {
    //  Function that decrements the page number by 1
    const prevPage = () => {
        if (currentPage > 0) {
            updatePage(currentPage - 1);
        }
    }

    // Function increments the page number by 1
    const nextPage = () => {
        if (currentPage + 1 < totalPages) {
            updatePage(currentPage + 1);
        }
    }

    return (
        <nav aria-label="Page navigation">
            <ul className="pagination justify-content-center">
                {/* Previous page button - dynamically applies the disabled class when page is 0, */}
                <li className={`page-item ${currentPage === 0 && 'disabled'}`}>
                    <button className="page-link" onClick={prevPage} aria-label="Previous" disabled={currentPage === 0}>
                        <GrFormPrevious />
                    </button>
                </li>
                {/* Current page - displays the current page number (adding 1 because page is zero-based) and the total number of pages.*/}
                <li className="page-item">
                    <span className="page-link">
                        {currentPage + 1} of {totalPages}
                    </span>
                </li>
                {/* Next page button - Disables the button when page + 1 is greater than or equal to totalPages to prevent navigation to the next page when already on the last page.*/}
                <li className={`page-item ${currentPage + 1 >= totalPages && 'disabled'}`}>
                    <button className="page-link" onClick={nextPage} aria-label="Next" disabled={currentPage + 1 >= totalPages}>
                        <GrFormNext />
                    </button>
                </li>
            </ul>
        </nav>
    );
}

export default Pagination;



