import requests
import json
from open_library_identifiers import author_identifiers
googleApiKey = 'AIzaSyCmvxWGLO801yhTy0O5T-CjQpLo2s813BI'
cx = '305ebc0d18267427c'

def fetch_image_url(query, num=1):
    res = ''
    img_api_url = f"https://www.googleapis.com/customsearch/v1?key={googleApiKey}&cx={cx}&searchType=image&q={query}&num={num}"
    response = requests.get(img_api_url)
    data = response.json()

    if "items" in data:
        res = data["items"][0]["link"]
    return res


def fetch_book_by_author(author_name):
    url = f'https://www.googleapis.com/books/v1/volumes?q=inauthor:{author_name}&maxResults=3'
    response = requests.get(url)
    return response.json()

def save_to_json(data, filename):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=4)

def collect_books_for_authors():
    books_data = []
    for author_name in author_identifiers.keys():
        author_books = fetch_book_by_author(author_name)
        if 'items' in author_books and author_books['items']:
            for i in range(3):
                oneBook = {}
                volumeInfo = author_books['items'][i]['volumeInfo']
                oneBook['title'] = volumeInfo['title']
                oneBook['publish_date'] = volumeInfo.get('publishedDate', 'N/A')
                oneBook['page_count'] = volumeInfo.get('pageCount', 0)
                oneBook['publisher_name'] = volumeInfo.get('publisher', 'N/A')
                oneBook['description'] = volumeInfo.get('description', 'N/A')
                oneBook['author'] = ', '.join(volumeInfo['authors'])
                if 'imageLinks' in volumeInfo:
                    if 'thumbnail' in volumeInfo['imageLinks']:
                        oneBook['imageLink'] = volumeInfo['imageLinks']['thumbnail']
                    elif 'smallThumbnail' in volumeInfo['imageLinks']:
                        oneBook['imageLink'] = volumeInfo['imageLinks']['smallThumbnail']
                else:
                    oneBook['imageLink'] = "No Link Available"
                books_data.append(oneBook)
    return books_data

books_data = collect_books_for_authors()
save_to_json(books_data, 'books.json')
