import React from 'react';

const Dropdown = ({ value, onChange, options, className }) => {
  const dropdownStyle = {
    backgroundColor: '#925e48', // Brown color
    color: '#fff', // White text color
    border: 'none', // No border
    padding: '8px 12px', // Padding for the dropdown
    borderRadius: '4px', // Rounded corners
    cursor: 'pointer', // Pointer cursor
    minWidth: '150px', // Minimum width for consistency
    fontSize: '14px', // Font size
  };

  const optionStyle = {
    color: '#000', // Black text color for options
  };

  return (
    <select
      value={value}
      onChange={(e) => onChange(e.target.value)}
      style={dropdownStyle} // Apply styles directly
      className={className} // Apply any additional class name
    >
      {options.map((option, index) => (
        <option key={index} value={option.value} style={optionStyle}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export default Dropdown;
