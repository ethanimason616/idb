import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import urls from '../url.json';

function OnePublisher() {
    const { publisherId } = useParams();
    const [publisher, setPublisher] = useState(null);
    const [fetchSuccess, setFetchSuccess] = useState(false);
    const [bookLink, setBookLink] = useState(null);
    const [authorLink, setAuthorLink] = useState(null);

    useEffect(() => {
        const fetchPublisher = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/publishers/${publisherId}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch publisher data');
                }
                const publisherData = await response.json();
                setPublisher(publisherData);
                setFetchSuccess(true);
            } catch (error) {
                console.error('Error fetching publisher data:', error);
            }
        };

        const fetchBookLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/publishers/${publisherId}/bookid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const bookID  = jsonData.id;
                setBookLink(`${urls.frontendURL}books/${bookID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        const fetchAuthorLink = async () => {
            try {
                const response = await fetch(`${urls.backendURL}api/publishers/${publisherId}/authorid`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                const authorID  = jsonData.id;
                setAuthorLink(`${urls.frontendURL}authors/${authorID}`);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchPublisher();
        fetchBookLink();
        fetchAuthorLink();

    }, [publisherId]);

    return (
        <div className='page-container'>
            {!fetchSuccess ? (
                <p>Loading...</p>
            ) : !publisher ? (
                <p>No data found for this publisher.</p>
            ) : (
                <>
                    <h1>{publisher.name}</h1>
                    <br></br>
                    <img src={publisher.imageLink} alt="Publisher Image" />
                    <br></br>
                    <br></br>
                    <p>Country: {publisher.country}</p>
                    <p>Average Rating: {publisher.averageRating}</p>
                    <p>Book Count: {publisher.totalItems}</p>
                    <p>Link to Books: <a href={publisher.allBooksLink}>{publisher.allBooksLink}</a></p>
                    <p>Link to Purchase: <a href={publisher.buyLink}>{publisher.buyLink}</a></p>
                    <p>One Book by This Publisher: <a href={bookLink}>{bookLink}</a></p>
                    <p>One Author That Used This Publisher: <a href={authorLink}>{authorLink}</a></p>
                </>
            )}
        </div>
    );
}

export default OnePublisher;
