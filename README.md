**Canvas Group Number: 1**

**Team Members:**

Ethan Mason
Sohil Patel
Kathy Liang
Nerwen Cao
Hannah Hughes


**Project name:**
BookLore

**Website:**
https://booklore.software/

**API URL:**
https://database.booklore.software/api

**Postman Documentation:**
https://booklore.software/


**Git SHA:**
6f5329c285b9b36077468ca5653585fe5beb0bb9

**The proposed project:**

Our application will allow users to explore a vast database of books, authors, and publishers. Users can search for books by title, author, genre, and publication year, and view detailed information about authors. The interconnected nature of the models will facilitate easy navigation between related pages, providing a seamless user experience.

**Data Sources:**


https://openlibrary.org/developers/api OpenLibrary API

https://developers.google.com/books/docs/v1/using OpenLibrary API (books)

https://openlibrary.org/dev/docs/api/authors OpenLibrary API (authors)

https://openlibrary.org/dev/docs/api/subjects OpenLibrary API (subjects)

https://developers.google.com/books Google Books API

https://www.bing.com/images/search Used to get images 

**Models:**

Books

Authors 

Publishers


Estimate of the number of instances of each model:

Books: ~ 300
Authors: ~ 100
Publishers: ~ 100


**Each model must have many attributes. Describe five of those attributes for each model that you can filter or sort:**

_Books_

Title (Sortable)

Publish Date

Page Count (Filterable)

Description

Author

Publisher Name

Image


_Authors_

Name (sortable)

Birth Date (filterable)

Death Date (filterable)

Work Count (sortable)

Bio 

Image

Nationality (sortable)


_Publishers_

Name (sortable)

Link to purchase

Link to books published

Average Rating (sortable)

Total Items (sortable)

Image

Country (sortable)




**Instances of each model must connect to instances of at least two other models:**

_Books_ 

Linked to author who wrote them

Linked to publisher that published them


_Authors_

Linked to book(s) written by them

Linked to publishers(s) used by them


_Publishers_

Linked to books they published

Linked to authors they published