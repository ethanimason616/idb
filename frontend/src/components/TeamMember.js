import React from 'react';
import '../styles/TeamMember.css'

const TeamMember = ({ name, role, description, image, gitLabID, issuesClosed, commits, unitTests }) => {
  return (
    <div className="card">
      <img className="card-img-top headshot" src={image} alt="Card image"/>
      <div className="card-body">
        <h4 className="card-title">{name}</h4>
        <h6 className="card-text">Role: {role}</h6>
        <p className="card-text">{description}</p>
      </div>
      <div className="card-footer">
        <p className="card-text">GitLab ID: @{gitLabID}</p>
        <p className="card-text">Issues Closed: {issuesClosed}</p>
        <p className="card-text">Commits: {commits}</p>
        <p className="card-text">Unit Tests: {unitTests}</p>
      </div>
    </div>
  );
};

export default TeamMember;