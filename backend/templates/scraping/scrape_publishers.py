import requests
import json
from publisher_names import publishers
from bs4 import BeautifulSoup
from urllib.parse import quote


def fetch_total_items(publisher):
    url = f'https://www.googleapis.com/books/v1/volumes?q=inpublisher:{publisher}&maxResults=40'
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()  # assuming data is a dictionary with a 'size' field
        totalItems = data.get('totalItems')
        if totalItems is not None:
            return totalItems
    return 0

def fetch_link(publisher):
    publisher = publisher.replace(' ', '+')
    return f'https://books.google.com/books?q={publisher}'

def fetch_buy_link(publisher):
    url = f'https://www.googleapis.com/books/v1/volumes?q=inpublisher:{publisher}&maxResults=1'
    response = requests.get(url)
    
    if response.status_code == 200:
        data = response.json()
        items = data.get('items', [])
        
        if items:
            buy_link = items[0].get('saleInfo', {}).get('buyLink')
            if buy_link:
                return buy_link 
        
    return "Unavailable on Google Play Store"


def fetch_all_books_by_publisher(publisher):
    all_books = []
    startIndex = 0
    totalItems = 0

    while True:
        url = f"https://www.googleapis.com/books/v1/volumes?q=inpublisher:{publisher}&startIndex={startIndex}&maxResults=40"
        response = requests.get(url)
        data = response.json()

        books = data.get('items', [])
        totalItems = data.get('totalItems', 0)

        all_books.extend(books)
        startIndex += 40

        if len(books) < 40:
            break

    return all_books

def fetch_average_rating(books):
    # Filter out books without ratings
    rated_books = [book for book in books if 'averageRating' in book['volumeInfo']]

    total_rating = sum(book['volumeInfo']['averageRating'] for book in rated_books)
    average_rating = total_rating / len(rated_books) if rated_books else 0

    return round(average_rating, 1)


def fetch_image_url(query, num=1):
    query = quote(query) + 'publisher'
    search_url = f"https://www.bing.com/images/search?q={query}&first={num}"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
    }
    
    response = requests.get(search_url, headers=headers)
    soup = BeautifulSoup(response.content, 'html.parser')
    
    img_tags = soup.findAll('img', class_='mimg')
    if img_tags:
        return img_tags[0]['src']
    
    return ''



def save_to_json(data, filename):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=4)


def get_all_data():
    publishers_data = []
    for publisher in publishers:
        onePublisher = {}
        onePublisher['name'] = publisher
        onePublisher['totalItems'] = fetch_total_items(publisher)
        onePublisher['allBooksLink'] = fetch_link(publisher)
        onePublisher['buyLink'] = fetch_buy_link(publisher)
        booksByPublisher = fetch_all_books_by_publisher(publisher)
        onePublisher['averageRating'] = fetch_average_rating(booksByPublisher)
        onePublisher['imageLink'] = fetch_image_url(publisher)
        publishers_data.append(onePublisher)
    
    return publishers_data

publishers_data = get_all_data()
save_to_json(publishers_data, 'publishers.json')
